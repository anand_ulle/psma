# import all the necessary packages 
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier

# Import data
df = pd.read_csv("PRAD_Tumor_Samples_mRNA_RAW-Target_Comb_GS.csv", low_memory=False)
# remove class 10,6,8 Gleason.Score as the number of cases are less compared to class 7 and class 9 [Gleason.Score]
df = df[df['Gleason.Score'] != 10]
df = df[df['Gleason.Score'] != 6]
df = df[df['Gleason.Score'] != 8]

# Get the independent variables in X and the dependent variable Y (Gleason.Score)
# new_df = df.drop(['Unnamed: 0'],axis=1)
Y = new_df.iloc[:,0]
X = new_df.iloc[:,1:-1]

# Check for class imbalance
print(df.groupby(Y).size())

# Normalize features within range 0 (minimum) and 1 (maximum)
scaler = MinMaxScaler(feature_range=(0, 1))
X = scaler.fit_transform(X)
X = pd.DataFrame(X)

# Convert target Y to one hot encoded Y for Neural Network
Y = pd.get_dummies(Y)

# For Keras, convert dataframe to array values (Inbuilt requirement of Keras)
X = X.values
Y = Y.values

# First define baseline model. Then use it in Keras Classifier for the training
def baseline_model():
    # Create model here
    model = Sequential()
    model.add(Dense(1020, input_dim = 20530, activation = 'relu'))
    # model.add(Dense(32, activation = 'relu'))
    model.add(Dense(2, activation = 'softmax')) # Softmax for multi-class classification
    # Compile model here
    model.compile(loss = 'categorical_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
    return model
    
# Create Keras Classifier and use predefined baseline model
estimator = KerasClassifier(build_fn = baseline_model, epochs = 20, batch_size = 10, verbose = 0)
# Try different values for epoch and batch size

# KFold Cross Validation
kfold = KFold(n_splits = 5, shuffle = True)
# Try different values of splits e.g., 10

# Object to describe the result
results = cross_val_score(estimator, X, Y, cv = kfold)
# Result
print("Result: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))